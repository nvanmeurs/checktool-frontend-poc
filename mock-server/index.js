/* eslint-disable no-underscore-dangle, global-require, import/no-dynamic-require, no-restricted-syntax */
const express = require('express');
const fs = require('fs');
const http = require('http');

const app = express();

/* Generate a route for every fixture in the fixtures dir
   Route path is based on filename convention `__` becomes `/` */
for (const fileName of fs.readdirSync('mock-server/fixtures')) {
  const apiPath = fileName.split('.')[0].replace(/__/g, '/');

  app.get(`/${apiPath}/`, (req, res) =>
    res.json(require(`./fixtures/${fileName}`))
  );
}

/* List all available routes */
app.get('/', (req, res) =>
  res.json(
    app._router.stack
      .filter(entry => entry.route)
      .map(entry => entry.route.path)
  )
);

const server = http.createServer(app);

server.listen(3000);
