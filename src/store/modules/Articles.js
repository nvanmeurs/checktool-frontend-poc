const initialState = {
  articles: [
    {
      id: 1,
      title: 'Success!',
      date: '05/10/2017 - 09:00:42',
      content:
        'Succes vandaag met de start van Aan de slag met Exact Online. Voor vragen kun je telefonisch of per mail contact opnemen : 06 22 72 42 88 of natasja@tree9.nl'
    },
    {
      id: 2,
      title: 'Success!',
      date: '05/10/2017 - 09:00:42',
      content:
        'Succes vandaag met de start van Aan de slag met Exact Online. Voor vragen kun je telefonisch of per mail contact opnemen : 06 22 72 42 88 of natasja@tree9.nl'
    },
    {
      id: 3,
      title: 'Success!',
      date: '05/10/2017 - 09:00:42',
      content:
        'Succes vandaag met de start van Aan de slag met Exact Online. Voor vragen kun je telefonisch of per mail contact opnemen : 06 22 72 42 88 of natasja@tree9.nl'
    }
  ]
};

const getters = {};

const mutations = {};

const actions = {};

export default {
  namespaced: true,
  state: initialState,
  getters,
  mutations,
  actions
};
