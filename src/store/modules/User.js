const initialState = {
  id: window.userId,
  role: window.userRole
};

const getters = {
  isTeacher({ role }) {
    return role === 'teacher';
  },

  isStudent({ role }) {
    return role === 'student';
  }
};

const mutations = {};

const actions = {};

export default {
  namespaced: true,
  state: initialState,
  getters,
  mutations,
  actions
};
