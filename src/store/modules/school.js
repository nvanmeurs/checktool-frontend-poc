const initialState = {
  classes: [
    {
      id: 1,
      name: '1A'
    },
    {
      id: 2,
      name: '1B'
    },
    {
      id: 2,
      name: '1C'
    }
  ]
};

const getters = {};

const mutations = {};

const actions = {};

export default {
  namespaced: true,
  state: initialState,
  getters,
  mutations,
  actions
};
