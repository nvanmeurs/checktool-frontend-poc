import router from '@/router';

const initialState = {
  isMenuExpanded: false,
  menuItems: [
    {
      icon: 'home',
      route: {
        name: 'articles'
      },
      name: 'home',
      text: 'Home',
      requiredRole: null
    },
    {
      icon: 'ios-people',
      route: {
        name: 'classes'
      },
      name: 'classes',
      text: 'Classes',
      requiredRole: 'teacher'
    }
  ]
};

const getters = {
  menuItems({ menuItems }, _getters, { user }) {
    return menuItems.filter(
      ({ requiredRole }) => requiredRole === null || requiredRole === user.role
    );
  },

  menuColSpan({ isMenuExpanded }) {
    return isMenuExpanded ? 4 : 1;
  },

  contentColSpan({ isMenuExpanded }) {
    return isMenuExpanded ? 20 : 23;
  },

  menuItemIconSize({ isMenuExpanded }) {
    return isMenuExpanded ? 14 : 24;
  },

  getMenuItemByName({ menuItems }) {
    return menuItemName => {
      const [menuItem] = menuItems.filter(({ name }) => name === menuItemName);
      return menuItem;
    };
  },

  activeMenuItemName({ menuItems }, _getters, { route }) {
    const [{ name }] = menuItems.filter(
      menuItem => menuItem.route.name === route.name
    );

    return name;
  }
};

const mutations = {
  toggleMenu(state) {
    state.isMenuExpanded = !state.isMenuExpanded;
  }
};

const actions = {
  onMenuItemSelect({ getters: { getMenuItemByName } }, menuItemName) {
    const { route } = getMenuItemByName(menuItemName);
    router.push(route);
  }
};

export default {
  namespaced: true,
  state: initialState,
  getters,
  mutations,
  actions
};
