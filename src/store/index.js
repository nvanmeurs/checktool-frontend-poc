import Vue from 'vue';
import Vuex from 'vuex';

import app from './modules/app';
import user from './modules/user';
import articles from './modules/articles';
import school from './modules/school';

Vue.use(Vuex);

export default new Vuex.Store({
  modules: { app, user, articles, school },
  strict: process.env.NODE_ENV !== 'production'
});
