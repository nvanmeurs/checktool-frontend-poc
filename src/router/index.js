import Vue from 'vue';
import Router from 'vue-router';
import Articles from '@/components/Articles';
import Classes from '@/components/Classes';

Vue.use(Router);

export default new Router({
  routes: [
    {
      path: '/',
      name: 'articles',
      component: Articles
    },
    {
      path: '/classes',
      name: 'classes',
      component: Classes
    }
  ]
});
